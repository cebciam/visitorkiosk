package com.xeminent.zerox.Utils;

public interface Constants {

    String BASE_URL = "https://vmwappsha01prsc.sisa.samsung.com/";

    String CLOUD_PRINT_URL = "https://www.google.com/cloudprint/";


    String WEB_URL = "http://example.com/api/";

    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String EXCEPTION = "Exception";
    String EXCEPTION_MESSAGE = "Something went wrong";
    String SERVER_EXCEPTION_MESSAGE = "Something went wrong, server not responding";

    // Snappy save User data
    String USER_DATA = "userData";
    // Preferences
    String PREF_IS_USER_LOGIN = "isUserLogIn";
    String PREF_IS_LOCATION_ADDED = "isLocationAdded";

    String TYPE = "type";
    String USER = "user";
    String EMPLOYEE = "employee";
    String BEARER = "Bearer ";
    String CURRENT_CITY_NAME = "currentCityName";
    String CURRENT_CITY_ID = "currentCityId";

    String PREF_LANGUAGE = "language";
    String PREF_PRINTER = "printer";
    String PREF_LOCATION = "location";
}
