package com.xeminent.zerox.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.Toast;
import com.xeminent.zerox.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import dmax.dialog.SpotsDialog;

public class Util {
    private static final String TAG = Util.class.getName();

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null)
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
        }
        return false;
    }

    public static void showToastMsg(Context context, String msg) {

        try {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }
    }

    public static Boolean isValidURL(String URL) {
        return Patterns.WEB_URL.matcher(URL).matches();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidMobile(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.PHONE.matcher(target).matches();
    }

    public static void playRingTone(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone ringtone = RingtoneManager.getRingtone(context, notification);
            if (!ringtone.isPlaying()) {
                ringtone.play();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(Context context, EditText input) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void hideKeyboard(Context context, Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            view = new View(activity);
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Context context, EditText input) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(input, 0);
        } catch (Exception e) {
        }
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public static void showSettingsAlert(final Activity activity) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        alertDialog.setTitle("Enable GPS");

        alertDialog.setMessage("GPS is not enabled. Please go to setting and turn on GPS");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivityForResult(intent, 102);
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
//    public static Dialog loadingDialog(Context mContext) {
//        Dialog pd = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar);
//        View view = LayoutInflater.from(mContext).inflate(R.layout.progress_dialog, null);
//        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        pd.setContentView(view);
//        return pd;
//    }

//    public static void showProgressBar(Context context) {
//        try {
//            Dialog progressBar = loadingDialog(context);
//            progressBar.show();
//        } catch (Exception e) {
//
//        }
//    }

    public static boolean isValidString(String value) {
        return !(value == null || value.isEmpty());
    }

    public static boolean containsEqualIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null)
            return false;
        if (str.equalsIgnoreCase(searchStr)) {
            return true;
        }
        final int length = searchStr.length();
        if (length == 0)
            return true;

        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }

    public static void launchURL(Context context, String url) {
        if (!url.equals("") && URLUtil.isValidUrl(url)) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        }
    }


    public static void launchYoutube(Context context, String videoId) {
        //String videoId = "Fee5vbFLYM4";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + videoId));
        intent.putExtra("VIDEO_ID", videoId);
        context.startActivity(intent);
    }

    public static void launchShareIntent(Context context, String text) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    private static String capitalizeAllWords(String str) {
        StringBuilder phrase = new StringBuilder();
        boolean capitalize = true;
        for (char c : str.toLowerCase().toCharArray()) {
            if (Character.isLetter(c) && capitalize) {
                phrase.append(Character.toUpperCase(c));
                capitalize = false;
                continue;
            } else if (c == ' ') {
                capitalize = true;
            }
            phrase.append(c);
        }
        return phrase.toString();
    }

    public static boolean containsIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null)
            return false;

        final int length = searchStr.length();
        if (length == 0)
            return true;

        if (str.contains(searchStr))
            return true;

       /* for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }*/
        return false;
    }

    public static void setLocale(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static String convertMilliSecondToMinutes(long seconds) {
        long hours = TimeUnit.SECONDS.toHours(seconds);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds - TimeUnit.HOURS.toSeconds(hours));
        long remainSeconds = seconds - TimeUnit.MINUTES.toSeconds(minutes) - TimeUnit.HOURS.toSeconds(hours);
        String result = String.format("%02d", hours) + ":"
                + String.format("%02d", minutes) + ":" + String.format("%02d", remainSeconds);
        return result;
    }

    public static AlertDialog progressDialog(Context context) {
        AlertDialog dialog = new SpotsDialog(context, R.style.dialog_style);
        return dialog;
    }


    public static String parseDate(String givenDateString) {
        String result = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date past = format.parse(givenDateString);
            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                result = "just now";
            } else if (minutes < 60) {
                result = minutes + " minutes ago";
            } else if (hours < 24) {
                result = hours + " hours ago";
            } else {
                if (days > 30) {
                    int x = (int) (days / 30);
                    result = x + " Months ago";
                } else if (days > 365) {
                    int x = (int) (days / 365);
                    result = x + " years ago";
                } else {
                    result = days + " days ago";
                }
            }
        } catch (Exception j) {
            j.printStackTrace();
        }

        return result;
    }


    public static boolean isUserLogin(Context context) {
        if (Utilities.getInstance(context).getBooleanPreferences(Constants.PREF_IS_USER_LOGIN)) {
            return true;
        }
        return false;
    }


    public static String getCurrentTimeSpan() {
        Date d = new Date();
        CharSequence s = android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        return s.toString();
    }
}
