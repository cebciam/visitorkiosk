package com.xeminent.zerox;

import android.app.Application;
import android.content.Context;
import android.os.PowerManager;
import android.view.WindowManager;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;
import com.xeminent.zerox.NetWorkClient.ApiClient;
import com.xeminent.zerox.NetWorkClient.ApiInterface;


public class AppController extends Application {
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private static AppController mInstance;
    private static ApiInterface apiService;
    private static DB snappy = null;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        configRetrofit();
        acquireWakeLock();
    }

    private void acquireWakeLock() {
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::MyWakelockTag");
        wakeLock.acquire();
    }

    private static void configRetrofit() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static ApiInterface getApiService() {
        return apiService;
    }


    public static synchronized DB getSnappyInstance() {
        try {
            if (snappy == null) {
                snappy = DBFactory.open(mInstance);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return snappy;
    }
}
