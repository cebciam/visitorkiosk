package com.xeminent.zerox.NetWorkClient;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.xeminent.zerox.Models.CheckIn;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("VisitorManagementKiosk/api/GetVisitor")
    Call<ArrayList<CheckIn>> getCheckInResponse(@Query("code") String code);


//    @FormUrlEncoded
    @GET("WifiRequestApi/api/GetValidUser")
    Call<JsonArray> getHostVerificationResponse(@Query("knoxid") String hostEmail);

    //    @FormUrlEncoded
    @Headers("Content-Type:application/json")
    @POST("VisitorManagementKiosk/api/CheckInVisitor")
    Call<String> getSubmitFormResponse(@Body JsonObject body);


}
