package com.xeminent.zerox.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.xeminent.zerox.Activities.AdminArea.SelectPrinter;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Utilities;

public class AskPreRegistration extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {


    private CardView cvYes, cvNo;
    private int count = 0;
    private ViewGroup root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_pre_registration);
//        if (Utilities.getInstance(AskPreRegistration.this).getStringPreferences(Constants.PREF_PRINTER).isEmpty()) {
//            startActivity(new Intent(AskPreRegistration.this, SelectPrinter.class));
//        }
        initial();
    }


    private void initial() {

        cvYes = findViewById(R.id.cvYes);
        cvYes.setOnClickListener(this);
        cvNo = findViewById(R.id.cvNo);
        cvNo.setOnClickListener(this);

        root = findViewById(R.id.root);
        root.setOnTouchListener(this);

        startTimer();
    }


    private void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (count >= 30) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
                    return;
                }
                count++;
                startTimer();
            }
        }, 1000);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cvYes: {
                startActivity(new Intent(AskPreRegistration.this, VisitorCheckIn.class));
                break;
            }
            case R.id.cvNo: {
                startActivity(new Intent(AskPreRegistration.this, PurposeOfVisit.class));
                break;
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        count = 0;
        return false;
    }
}
