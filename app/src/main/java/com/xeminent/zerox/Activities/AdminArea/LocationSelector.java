package com.xeminent.zerox.Activities.AdminArea;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.xeminent.zerox.Activities.VisitorDetailsForm;
import com.xeminent.zerox.AppController;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Util;
import com.xeminent.zerox.Utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSelector extends AppCompatActivity implements View.OnClickListener {

    private Spinner spnLocations;
    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selector);

        initial();
    }

    private void initial() {
        spnLocations = findViewById(R.id.spnLocations);
        setSpinnerAdapter();
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);

    }


    private void setSpinnerAdapter() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.location_array));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnLocations.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onClick(View v) {

        Utilities.getInstance(LocationSelector.this).saveStringPreferences(Constants.PREF_LOCATION, spnLocations.getSelectedItem().toString());
        Util.showToastMsg(LocationSelector.this, "Location Updated Successfully");
        finish();
//        if (!Util.isConnectingToInternet(this)) {
//            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
//            return;
//        }
//        Call<JsonObject> call = AppController.getInstance().getApiService().getHostVerificationResponse(spnLocations.getSelectedItem().toString());
//        call.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                if (response.code() == 200) {
//                    try {
//
//
//                    } catch (Exception e) {
//                        Log.e(Constants.EXCEPTION, e.getMessage());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                Log.e(Constants.EXCEPTION, t.getMessage());
//                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
//            }
//        });
    }
}
