package com.xeminent.zerox.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.xeminent.zerox.Models.CheckIn;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Util;
import com.xeminent.zerox.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

public class UserDetails extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private TextView tvAddress, tvName, tvHost, tvValidDate, tvStatus, tvEscort;
    private ImageButton ibBack, ibPrint;
    private CheckIn userDetails;
    private String imagesUri, path, file_name = "Screenshot", email, authToken, accessToken ,  authId, userName;
    private Bitmap b;
    private int totalHeight, totalWidth;
    private File myPath;
    private GoogleApiClient mGoogleApiClient;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        initGoogle();
        initial();
    }

    private void initGoogle()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.gg_client_web_id))
                .requestEmail()
                .requestServerAuthCode(getString(R.string.gg_client_web_id))
                .requestScopes(new Scope("https://www.googleapis.com/auth/cloudprint"))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    private void getAccess(String code) {
        String url = "https://www.googleapis.com/oauth2/v4/token";
        Ion.with(this)
                .load("POST", url)
                .setBodyParameter("client_id", getString(R.string.gg_client_web_id))
                .setBodyParameter("client_secret", getString(R.string.gg_client_web_secret))
                .setBodyParameter("code", code)
                .setBodyParameter("grant_type", "authorization_code")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result)
                    {
                        if (e == null)
                        {
                            try
                            {
                                JSONObject json = new JSONObject(result.getResult());
                                accessToken  = json.getString("access_token");
                                printPdf(path, Utilities.getInstance(UserDetails.this).getStringPreferences(Constants.PREF_PRINTER));
                            }
                            catch (JSONException e1)
                            {
                                dialog.dismiss();
                                Util.showToastMsg(UserDetails.this, getResources().getString(R.string.accesstoken_exception));
                            }
                        }
                        else
                        {
                        dialog.dismiss();
                        Util.showToastMsg(UserDetails.this, getResources().getString(R.string.accecctoken_problem));
                    }
                    }
                });
    }
    private void initial() {
        userDetails = (CheckIn) getIntent().getSerializableExtra("user");

        ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(this);
        ibPrint = findViewById(R.id.ibPrint);
        ibPrint.setOnClickListener(this);

        tvAddress = findViewById(R.id.tvAddress);
        tvName = findViewById(R.id.tvName);
        tvHost = findViewById(R.id.tvHost);
        tvValidDate = findViewById(R.id.tvValidDate);
        tvEscort = findViewById(R.id.tvEscort);
        tvStatus = findViewById(R.id.tvStatus);

       populateData();
    }

    private void populateData()
    {
        tvAddress.setText(userDetails.getLocation());
        tvName.setText(userDetails.getNameBadge());
        tvHost.setText(userDetails.getVisiting());
        tvValidDate.setText(userDetails.getBudgeValidDate());
        tvEscort.setText(userDetails.getCText());
//        tvStatus.setText(userDetails.getLocation());

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try
                {
                    Thread.sleep(500);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ibPrint.callOnClick();
                        }
                    });
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibBack: {
                finish();
                break;
            }
            case R.id.ibPrint: {
                if (checkPermissions())
                {
                    dialog = Util.progressDialog(UserDetails.this);
                    dialog.show();
                    loginWithGoogle();
                    takeScreenShot();
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101)
        {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try
        {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
//            personPhoto = acct.getPhotoUrl().toString();
            userName = acct.getDisplayName();
            email = acct.getEmail();
            authToken = acct.getIdToken();
            authId = acct.getId();
            String code = acct.getServerAuthCode();
            getAccess(code);
        }
        catch (ApiException e)
        {
            dialog.dismiss();
            Util.showToastMsg(UserDetails.this, getResources().getString(R.string.google_login_exception));
        }
    }

    private void loginWithGoogle() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 101);

//        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//        startActivityForResult(signInIntent, 101);
    }

    private void printPdf(String pdfPath, String printerId) {
        String url = Constants.CLOUD_PRINT_URL + "submit";
        Ion.with(this)
                .load("POST", url)
                .addHeader("Authorization", "Bearer " + accessToken)
                .setMultipartParameter("printerid", printerId)
                .setMultipartParameter("title", "uzair")
                .setMultipartFile("content", "application/pdf", new File(pdfPath))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        if (e == null)
                        {
                            dialog.dismiss();
//                            Util.showToastMsg(UserDetails.this, getResources().getString(R.string.success_checkin));
                            startActivity(new Intent(UserDetails.this, ThankYou.class));
                            finish();
                        }
                        else
                        {
                            dialog.dismiss();
                            Util.showToastMsg(UserDetails.this, getResources().getString(R.string.google_print_expection));
                        }
                    }
                });
    }
    private void takeScreenShot() {

        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zerox/");

        if (!folder.exists()) {
            boolean success = folder.mkdir();
        }

        path = folder.getAbsolutePath();
        path = path + "/" + "file" + ".pdf";

        View u = findViewById(R.id.root);

        ScrollView z = (ScrollView) findViewById(R.id.root);
        totalHeight = z.getChildAt(0).getHeight();
        totalWidth = z.getChildAt(0).getWidth();

        Log.e("totalHeight--", "" + totalHeight);
        Log.e("totalWidth--", "" + totalWidth);

        //Save bitmap
        String extr = Environment.getExternalStorageDirectory() + "/ScreenShot/";
        File file = new File(extr);
        if (!file.exists())
            file.mkdir();
        String fileName = file_name + ".jpg";
        myPath = new File(extr, fileName);
        imagesUri = myPath.getPath();
        FileOutputStream fos = null;
        b = getBitmapFromView(u, totalHeight, totalWidth);

        try {
            fos = new FileOutputStream(myPath);
            b.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        createPdf();


    }

    public Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    private void createPdf() {

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(b.getWidth(), b.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();


        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);

        Matrix matrix = new Matrix();

        matrix.postRotate(180);
        Bitmap bitmap = Bitmap.createScaledBitmap(b, b.getWidth(), b.getHeight(), true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        paint.setColor(Color.BLUE);
        canvas.drawBitmap(rotatedBitmap, 0, 0, null);
        document.finishPage(page);
        File filePath = new File(path);
        try {
            document.writeTo(new FileOutputStream(filePath));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.somethingwrong) + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();


        if (myPath.exists())
            myPath.delete();

    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(UserDetails.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        Util.showToastMsg(UserDetails.this, connectionResult.getErrorMessage());
    }
}
