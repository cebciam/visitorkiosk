package com.xeminent.zerox.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.xeminent.zerox.Activities.AdminArea.LocationSelector;
import com.xeminent.zerox.Activities.AdminArea.SelectPrinter;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Utilities;

import java.util.Locale;

public class Splash extends AppCompatActivity implements View.OnClickListener {

    private TextView tvWelcome;
    private Button btnLanguage, btnCheckIn;
    private String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       // startActivity(new Intent(Splash.this, UserDetails.class));
        if (Utilities.getInstance(Splash.this).getStringPreferences(Constants.PREF_PRINTER).isEmpty()) {
            startActivity(new Intent(Splash.this, SelectPrinter.class));
        }
        initial();
    }

    private void initial() {
        lang = Utilities.getInstance(Splash.this).getStringPreferences(Constants.PREF_LANGUAGE);
        if (lang.isEmpty())
            lang = "en";
        btnCheckIn = findViewById(R.id.btnCheckIn);
        btnCheckIn.setOnClickListener(this);
        btnLanguage = findViewById(R.id.btnLanguage);
        btnLanguage.setOnClickListener(this);
        tvWelcome = findViewById(R.id.tvWelcome);
        tvWelcome.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showAdminDialog();
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckIn: {
                startActivity(new Intent(Splash.this, AskPreRegistration.class));
                break;
            }
            case R.id.btnLanguage: {
                showLanguageDialog();
                break;
            }
        }
    }

    private void setLocal() {
        Locale locale = new Locale(lang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        startActivity(new Intent(Splash.this, Splash.class));
        Utilities.getInstance(Splash.this).saveStringPreferences(Constants.PREF_LANGUAGE, lang);
        finish();
    }

    void showLanguageDialog() {
        final Dialog dialog = new Dialog(Splash.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_language);

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        final TextView tvEnglish = (TextView) dialog.findViewById(R.id.tvEnglish);
        final TextView tvKorean = (TextView) dialog.findViewById(R.id.tvKorean);
        setColor(tvEnglish, tvKorean);
        tvEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lang = "en";
                setLocal();
                setColor(tvEnglish, tvKorean);
            }
        });
        tvKorean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lang = "ko";
                setLocal();
                setColor(tvEnglish, tvKorean);
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    void showAdminDialog() {
        final Dialog dialog = new Dialog(Splash.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_admin);

        Button btnPrinter, btnLocation;
        btnPrinter = dialog.findViewById(R.id.btnPrinter);
        btnPrinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Splash.this, SelectPrinter.class));
                dialog.dismiss();
            }
        });
        btnLocation = dialog.findViewById(R.id.btnLocation);
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Splash.this, LocationSelector.class));
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }


    private void setColor(TextView tvEnglish, TextView tvKorean) {
        if (lang.equalsIgnoreCase("en")) {
            tvEnglish.setTextColor(ContextCompat.getColor(Splash.this, R.color.colorPrimary));
            tvKorean.setTextColor(Color.BLACK);
            tvEnglish.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lines, 0, R.drawable.ic_selected, 0);
            tvKorean.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lines, 0, 0, 0);
        } else {
            tvKorean.setTextColor(ContextCompat.getColor(Splash.this, R.color.colorPrimary));
            tvEnglish.setTextColor(Color.BLACK);
            tvKorean.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lines, 0, R.drawable.ic_selected, 0);
            tvEnglish.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lines, 0, 0, 0);
        }
    }
}
