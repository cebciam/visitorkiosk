package com.xeminent.zerox.Activities.AdminArea;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.appcompat.app.AppCompatActivity;

import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Util;
import com.xeminent.zerox.Utils.Utilities;

public class SelectPrinter extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private CheckBox cbBuildingOne, cbBuildingTwo;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_printer);

        initial();
    }

    private void initial()
    {
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        cbBuildingOne = findViewById(R.id.cbBuildingOne);
        cbBuildingTwo = findViewById(R.id.cbBuildingTwo);
        cbBuildingOne.setOnCheckedChangeListener(this);
        cbBuildingTwo.setOnCheckedChangeListener(this);

        if(Utilities.getInstance(SelectPrinter.this).getStringPreferences(Constants.PREF_PRINTER).equalsIgnoreCase(cbBuildingOne.getText().toString()))
        {
            cbBuildingOne.setChecked(true);
            return;
        }
        cbBuildingTwo.setChecked(true);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(buttonView.getId() == cbBuildingOne.getId() && isChecked)
        {
            Utilities.getInstance(SelectPrinter.this).saveStringPreferences(Constants.PREF_PRINTER, cbBuildingOne.getText().toString());
            cbBuildingTwo.setChecked(false);
            return;
        }
        Utilities.getInstance(SelectPrinter.this).saveStringPreferences(Constants.PREF_PRINTER, cbBuildingTwo.getText().toString());
        cbBuildingOne.setChecked(false);
    }

    @Override
    public void onClick(View v )
    {
        if(Utilities.getInstance(SelectPrinter.this).getStringPreferences(Constants.PREF_PRINTER).isEmpty())
        {
            Util.showToastMsg(SelectPrinter.this, "Please select printer first");
            return;
        }
        finish();
    }
}
