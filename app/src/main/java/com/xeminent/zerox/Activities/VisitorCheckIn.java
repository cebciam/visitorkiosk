package com.xeminent.zerox.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.xeminent.zerox.AppController;
import com.xeminent.zerox.Models.CheckIn;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Util;
import com.xeminent.zerox.Utils.Utilities;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorCheckIn extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private CheckBox cbAcceptTerms;
    private TextView tvTerms;
    private ImageButton ibScan;
    private Button btnCheckIn;
    private String userCode;
    private EditText etCode;
    private int count = 0;
    private ViewGroup root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_checkin);

        initial();
    }

    private void initial() {

        cbAcceptTerms = findViewById(R.id.cbAcceptTerms);
        tvTerms = findViewById(R.id.tvTerms);
        tvTerms.setOnClickListener(this);
        root = findViewById(R.id.root);
        root.setOnTouchListener(this);
        btnCheckIn = findViewById(R.id.btnCheckIn);
        btnCheckIn.setOnClickListener(this);
        ibScan = findViewById(R.id.ibScan);
        ibScan.setOnClickListener(this);

        etCode = findViewById(R.id.etCode);

        startTimer();
    }

    private void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (count >= 60) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setLocal();
                        }
                    });
                    return;
                }
                count++;
                startTimer();
            }
        }, 6000);
    }

    private void setLocal() {
        Locale locale = new Locale("en");
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        startActivity(new Intent(VisitorCheckIn.this, Splash.class));
        Utilities.getInstance(VisitorCheckIn.this).saveStringPreferences(Constants.PREF_LANGUAGE, "en");
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibScan: {
                if (!checkPermissions())
                    return;

                if (!cbAcceptTerms.isChecked()) {
                    Util.showToastMsg(VisitorCheckIn.this, getResources().getString(R.string.accept_terms_error));
                    return;
                }
                IntentIntegrator integrator = new IntentIntegrator(this);
//                integrator.setPrompt("Scan");
                integrator.setCameraId(1);
                integrator.setBeepEnabled(true);
                integrator.setOrientationLocked(false);
                //Override here
                integrator.setCaptureActivity(ScannerActivity.class);

                integrator.initiateScan();
                break;
            }
            case R.id.btnCheckIn: {

                if (!cbAcceptTerms.isChecked()) {
                    Util.showToastMsg(VisitorCheckIn.this, getResources().getString(R.string.accept_terms_error));
                    return;
                }
                userCode = etCode.getText().toString();
                if (!userCode.isEmpty() && checkPermissions())
                    userCheckIn();
                break;
            }
            case R.id.tvTerms: {

                dialogTermsAndConditions();
                break;
            }
        }
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(VisitorCheckIn.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null)
            return;
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        userCode = result.getContents();
        userCheckIn();
    }


    private void userCheckIn() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final AlertDialog dialog = Util.progressDialog(VisitorCheckIn.this);
        dialog.show();
        Call<ArrayList<CheckIn>> call = AppController.getInstance().getApiService().getCheckInResponse(userCode);
        call.enqueue(new Callback<ArrayList<CheckIn>>() {
            @Override
            public void onResponse(Call<ArrayList<CheckIn>> call, Response<ArrayList<CheckIn>> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    try {
                        Intent intent = new Intent(VisitorCheckIn.this, UserDetails.class);
                        intent.putExtra("user", response.body().get(0));
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                        Util.showToastMsg(getApplicationContext(), getResources().getString(R.string.code_exception));
                        Log.e(Constants.EXCEPTION, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CheckIn>> call, Throwable t) {
                dialog.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        count = 0;
        return false;
    }


    void dialogTermsAndConditions() {
        final Dialog dialog = new Dialog(VisitorCheckIn.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_terms_conditions);

        ImageButton btnClose = dialog.findViewById(R.id.btnClose);
        Button btnOk = dialog.findViewById(R.id.btnOk);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }
}
