package com.xeminent.zerox.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.xeminent.zerox.AppController;
import com.xeminent.zerox.Enums.VisitorFormType;
import com.xeminent.zerox.Models.CheckIn;
import com.xeminent.zerox.R;
import com.xeminent.zerox.Utils.Constants;
import com.xeminent.zerox.Utils.Util;
import com.xeminent.zerox.Utils.Utilities;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorDetailsForm extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, View.OnFocusChangeListener, View.OnTouchListener {

    //    k.mahmud@samsung.com
    private ProgressBar progressBar;
    private CheckBox cbForgotHost, cbAcceptTerms;
    private ViewGroup root, llVisitorDetails, llSarVisitorDetails, llHostDetails, llFindHost;
    private TextView tvReason, tvTerms, tvFindHost, tvWifiState, tvWifiAccess;
    private EditText etFirstName, etLastName, etEmail, etCompany, etReason, etHost, etEmailSar, etHostFirstName, etHostLastName;
    private String firstName, lastName, email, company, reason, host, hostFirstName, hostLastName;
    private boolean isHostVerified;
    private Button btnBack, btnSubmit;
    private SwitchCompat scWifi;
    private VisitorFormType visitorFormType;
    private AlertDialog dialog;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_details_form);

        initial();
    }



    private void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (count >= 90) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
                    return;
                }
                count++;
                startTimer();
            }
        }, 1000);
    }

    private void initial() {

        dialog = Util.progressDialog(VisitorDetailsForm.this);
//        ViewGroup
        llVisitorDetails = findViewById(R.id.llVisitorDetails);
        llSarVisitorDetails = findViewById(R.id.llSarVisitorDetails);
        llHostDetails = findViewById(R.id.llHostDetails);
        llFindHost = findViewById(R.id.llFindHost);
        root = findViewById(R.id.root);
        root.setOnTouchListener(this);

//        EditText
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etCompany = findViewById(R.id.etCompany);
        etReason = findViewById(R.id.etReason);
        etHost = findViewById(R.id.etHost);
        etEmailSar = findViewById(R.id.etEmailSar);
        etHostFirstName = findViewById(R.id.etHostFirstName);
        etHostLastName = findViewById(R.id.etHostLastName);

        etHost.setOnFocusChangeListener(this);

//        TextView
        tvReason = findViewById(R.id.tvReason);
        tvTerms = findViewById(R.id.tvTerms);
        tvTerms.setOnClickListener(this);
        tvFindHost = findViewById(R.id.tvFindHost);
        tvFindHost.setOnClickListener(this);

//        Wifi switch
        tvWifiAccess = findViewById(R.id.tvWifiAccess);
        tvWifiState = findViewById(R.id.tvWifiState);
        scWifi = findViewById(R.id.scWifi);
        scWifi.setOnCheckedChangeListener(this);

//        CheckBox
        cbForgotHost = findViewById(R.id.cbForgotHost);
        cbForgotHost.setOnCheckedChangeListener(this);
        cbAcceptTerms = findViewById(R.id.cbAcceptTerms);

//        ProgressBar
        progressBar = findViewById(R.id.progressBar);

//        Button
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        setVisibility();

        startTimer();
    }

    private void setVisibility() {
        visitorFormType = (VisitorFormType) getIntent().getSerializableExtra("type");

        switch (visitorFormType) {
            case Visitor: {

                tvReason.setVisibility(View.VISIBLE);
                etReason.setVisibility(View.VISIBLE);
                break;
            }
            case Vendor: {

                break;
            }
            case SamsungEmployee: {

                break;
            }
            case SRAEmployee: {
                tvWifiAccess.setVisibility(View.GONE);
                tvWifiState.setVisibility(View.GONE);
                scWifi.setVisibility(View.GONE);
                llVisitorDetails.setVisibility(View.GONE);
                llSarVisitorDetails.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    private boolean formValidation() {
        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        email = etEmail.getText().toString();
        company = etCompany.getText().toString();
        reason = etReason.getText().toString();
        host = etHost.getText().toString();
        hostFirstName = etHostFirstName.getText().toString();
        hostLastName = etHostLastName.getText().toString();

        if (firstName.isEmpty()) {
            etFirstName.setError(getResources().getString(R.string.required));
            etFirstName.requestFocus();
            return false;
        }
        if (lastName.isEmpty()) {
            etLastName.setError(getResources().getString(R.string.required));
            etLastName.requestFocus();
            return false;
        }
        if (visitorFormType == VisitorFormType.SamsungEmployee) {
            if (scWifi.isChecked()) {
                if (!email.contains("samsung.com")) {
                    etEmail.setError(getResources().getString(R.string.samsung_email_required));
                    etEmail.requestFocus();
                    return false;
                }
            }

        } else if (visitorFormType == VisitorFormType.Visitor || visitorFormType == VisitorFormType.Vendor) {

            if (scWifi.isChecked()) {
                if (!Util.isValidEmail(email)) {
                    etEmail.setError(getResources().getString(R.string.valid_email_required));
                    etEmail.requestFocus();
                    return false;
                }
            }
        }
//        else {
//            if (!Util.isValidEmail(email)) {
//                etEmail.setError(getResources().getString(R.string.valid_email_required));
//                etEmail.requestFocus();
//                return false;
//            }
//        }
        if (!cbForgotHost.isChecked()) {
            if (!Util.isValidEmail(host)) {
                etHost.setError(getResources().getString(R.string.verified_host_required));
                etHost.requestFocus();
                return false;
            }
        }
        if (cbForgotHost.isChecked()) {
            if (hostFirstName.isEmpty()) {
                etHostFirstName.setError(getResources().getString(R.string.required));
                etHostFirstName.requestFocus();
                return false;
            }
            if (hostLastName.isEmpty()) {
                etHostLastName.setError(getResources().getString(R.string.required));
                etHostLastName.requestFocus();
                return false;
            }
        }
        if (visitorFormType == VisitorFormType.Visitor) {

            if (reason.isEmpty()) {
                etReason.setError(getResources().getString(R.string.required));
                etReason.requestFocus();
                return false;
            }

        }
        if (!cbAcceptTerms.isChecked()) {
            showPolicyWarningDialog();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTerms: {
                dialogTermsAndConditions();
                break;
            }
            case R.id.tvFindHost: {
                verifiedHost(false, false);
                break;
            }
            case R.id.btnBack: {
                finish();
                break;
            }
            case R.id.btnSubmit: {
                submitForm();
                break;
            }
        }
    }

    private void submitForm() {
        switch (visitorFormType) {
            case Visitor: {
                if (formValidation())
                    submitVisitorForm();
                break;
            }
            case Vendor: {
                if (formValidation())
                    submitContractorForm();
                break;
            }
            case SamsungEmployee: {
                if (formValidation())
                    submitHQAffiliateForm();
                break;
            }
            case SRAEmployee: {
                host = etEmailSar.getText().toString();
                if (!Util.isValidEmail(host)) {
                    etEmailSar.setError(getResources().getString(R.string.valid_email_required));
                    etEmailSar.requestFocus();
                    return;
                }
                if (!cbAcceptTerms.isChecked()) {
                    showPolicyWarningDialog();
                    return;
                }
                submitSARForm();
                break;
            }
        }
    }

    private void verifiedHost(final boolean isFormSubmit, final boolean isSRA) {

        final EditText etContainer;
        final String errorMessage;
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }

        if (!isSRA) {
            etContainer = etHost;
            errorMessage = getResources().getString(R.string.host_not_found);
            host = etHost.getText().toString();
            if (host.isEmpty() || cbForgotHost.isChecked())
                return;
        } else {
            errorMessage = getResources().getString(R.string.user_not_found);
            etContainer = etEmailSar;
        }
        progressBar.setVisibility(View.VISIBLE);
        Call<JsonArray> call = AppController.getInstance().getApiService().getHostVerificationResponse(host);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressBar.setVisibility(View.GONE);

                try {
                    if (response.code() == 200) {
                        isHostVerified = true;
                        etContainer.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_verified, 0);
                        if (isFormSubmit)
                            submitForm();
                    } else {
                        dialog.dismiss();
                        isHostVerified = false;
                        etContainer.setError(errorMessage);
                        etContainer.requestFocus();
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    isHostVerified = false;
                    etContainer.setError(errorMessage);
                    etContainer.requestFocus();
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                isHostVerified = false;
                dialog.dismiss();
                progressBar.setVisibility(View.GONE);
                etContainer.setError(errorMessage);
                etContainer.requestFocus();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private void submitFormRequest(boolean isSRA) {

        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        dialog.show();
        if (!isHostVerified & !cbForgotHost.isChecked()) {
            verifiedHost(true, isSRA);
            return;
        }
        Call<String> call = AppController.getInstance().getApiService().getSubmitFormResponse(isSRA ? getSRAValues() : getBodyValues());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                progressBar.setVisibility(View.GONE);

                try {
                    if (response.code() == 200) {
                        startActivity(new Intent(VisitorDetailsForm.this, ThankYou.class));
                    } else {

                    }
                } catch (Exception e) {
                    isHostVerified = false;
                    etHost.setError(getResources().getString(R.string.verified_host_required));
                    etHost.requestFocus();
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                isHostVerified = false;
                progressBar.setVisibility(View.GONE);
                etHost.setError(getResources().getString(R.string.verified_host_required));
                etHost.requestFocus();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private JsonObject getSRAValues() {
        JsonObject object = new JsonObject();
        object.addProperty("Location", Utilities.getInstance(VisitorDetailsForm.this).getStringPreferences(Constants.PREF_LOCATION));
        object.addProperty("VisitorType", visitorFormType.toString());
        object.addProperty("VisitorEmail", host);
        return object;
    }

    private JsonObject getBodyValues() {
        JsonObject object = new JsonObject();
        object.addProperty("Location", Utilities.getInstance(VisitorDetailsForm.this).getStringPreferences(Constants.PREF_LOCATION));
        object.addProperty("VisitorType", visitorFormType.toString());
        object.addProperty("Wifi", scWifi.isChecked());
        object.addProperty("FullName", firstName + " " + lastName);
        object.addProperty("Host", cbForgotHost.isChecked() ? (hostFirstName + " " + hostLastName) : host);
        object.addProperty("FirstName", firstName);
        object.addProperty("Company", company);
        object.addProperty("LastName", lastName);
        object.addProperty("VisitorEmail", email);
        if (visitorFormType == VisitorFormType.Visitor)
            object.addProperty("ReasonForVisit", reason);

        return object;

    }

    private void submitHQAffiliateForm() {
        submitFormRequest(false);
    }

    private void submitContractorForm() {
        submitFormRequest(false);
    }

    private void submitVisitorForm() {
        submitFormRequest(false);
    }

    private void submitSARForm() {
        submitFormRequest(true);
    }


    void dialogTermsAndConditions() {
        final Dialog dialog = new Dialog(VisitorDetailsForm.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_terms_conditions);

        ImageButton btnClose = dialog.findViewById(R.id.btnClose);
        Button btnOk = dialog.findViewById(R.id.btnOk);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        switch (compoundButton.getId()) {
            case R.id.scWifi: {
                setWifiState(isChecked);
                break;
            }
            case R.id.cbForgotHost: {
                setHostDetailsVisit(isChecked);
                break;
            }
        }

    }

    private void setHostDetailsVisit(boolean isChecked) {
        if (isChecked) {
            llFindHost.setVisibility(View.GONE);
            llHostDetails.setVisibility(View.VISIBLE);
        } else {
            llFindHost.setVisibility(View.VISIBLE);
            llHostDetails.setVisibility(View.GONE);
        }

    }

    private void setWifiState(boolean isChecked) {
        if (isChecked)
            tvWifiState.setText(getResources().getString(R.string.wifi_is_on));
        else
            tvWifiState.setText(getResources().getString(R.string.wifi_is_off));
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (v.getId() == etHost.getId()) {
            if (!hasFocus) {
                verifiedHost(false, false);
            }
        }
    }

    private void showPolicyWarningDialog() {
        String message = getResources().getString(R.string.please_accept) + " " + getResources().getString(R.string.terms_and_conditions);
        new AlertDialog.Builder(VisitorDetailsForm.this)
                .setTitle(getResources().getString(R.string.warning))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                })
                .show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        count = 0;
        return false;
    }
}
