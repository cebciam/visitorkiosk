package com.xeminent.zerox.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.xeminent.zerox.Enums.VisitorFormType;
import com.xeminent.zerox.R;

public class PurposeOfVisit extends AppCompatActivity implements View.OnClickListener {

    private CardView cvVisitor, cvHQ, cvContractor, cvSra;
    private VisitorFormType visitorFormType;
    private ImageButton ibBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_purpose_of_visit);

        initial();
    }

    private void initial() {
        cvVisitor = findViewById(R.id.cvVisitor);
        cvVisitor.setOnClickListener(this);
        cvHQ = findViewById(R.id.cvHQ);
        cvHQ.setOnClickListener(this);
        cvContractor = findViewById(R.id.cvContractor);
        cvContractor.setOnClickListener(this);
        cvSra = findViewById(R.id.cvSra);
        cvSra.setOnClickListener(this);
        ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvVisitor: {
                visitorFormType = VisitorFormType.Visitor;
                startActivity(new Intent(PurposeOfVisit.this, VisitorDetailsForm.class).putExtra("type", visitorFormType));
                break;
            }
            case R.id.cvHQ: {
                visitorFormType = VisitorFormType.SamsungEmployee;
                startActivity(new Intent(PurposeOfVisit.this, VisitorDetailsForm.class).putExtra("type", visitorFormType));
                break;
            }
            case R.id.cvContractor: {
                visitorFormType = VisitorFormType.Vendor;
                startActivity(new Intent(PurposeOfVisit.this, VisitorDetailsForm.class).putExtra("type", visitorFormType));

                break;
            }
            case R.id.cvSra: {
                visitorFormType = VisitorFormType.SRAEmployee;
                startActivity(new Intent(PurposeOfVisit.this, VisitorDetailsForm.class).putExtra("type", visitorFormType));

                break;
            }
            case R.id.ibBack:
            {
                finish();
                break;
            }
        }
    }
}
