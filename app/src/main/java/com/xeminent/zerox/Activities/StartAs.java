package com.xeminent.zerox.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xeminent.zerox.R;

import org.w3c.dom.Text;

public class StartAs extends AppCompatActivity implements View.OnClickListener {

    private View btnCheckIn, btnCheckOut, btnCheckInQr, btnCheckInPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_as);

        initial();
    }

    private void initial() {
        btnCheckIn = findViewById(R.id.btnCheckIn);
        setRes(btnCheckIn, getResources().getString(R.string.check_in), R.drawable.ic_login);
        btnCheckIn.setOnClickListener(this);

        btnCheckOut = findViewById(R.id.btnCheckOut);
        setRes(btnCheckOut, getResources().getString(R.string.check_out), R.drawable.ic_logout);
        btnCheckOut.setOnClickListener(this);

        btnCheckInPin = findViewById(R.id.btnCheckInPin);
        setRes(btnCheckInPin, getResources().getString(R.string.check_in_pin), R.drawable.ic_keypad_green);
        btnCheckInPin.setOnClickListener(this);

        btnCheckInQr = findViewById(R.id.btnCheckInQr);
        setRes(btnCheckInQr, getResources().getString(R.string.check_in_qr), R.drawable.ic_keypad_red);
        btnCheckInQr.setOnClickListener(this);
    }

    private void setRes(View btnCheckIn, String buttonText, int buttonIcon) {
        ImageView imageView = btnCheckIn.findViewById(R.id.imageView);
        imageView.setImageResource(buttonIcon);
        TextView textView = btnCheckIn.findViewById(R.id.textView);
        textView.setText(buttonText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckIn: {
                startActivity(new Intent(StartAs.this, PurposeOfVisit.class));
                break;
            }
            case R.id.btnCheckOut: {

//                startActivity(new Intent(StartAs.this, VisitorCheckIn.class));
                break;
            }
            case R.id.btnCheckInPin: {
                startActivity(new Intent(StartAs.this, VisitorCheckIn.class));
                break;
            }
            case R.id.btnCheckInQr: {
                startActivity(new Intent(StartAs.this, VisitorCheckIn.class));
                break;
            }
        }
    }
}
