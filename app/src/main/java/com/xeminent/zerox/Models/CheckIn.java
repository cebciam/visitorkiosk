package com.xeminent.zerox.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckIn implements Serializable {

	@SerializedName("budgeValidDate")
	private String budgeValidDate;

	@SerializedName("visiting")
	private String visiting;

	@SerializedName("location")
	private String location;

	@SerializedName("cText")
	private String cText;

	@SerializedName("name_badge")
	private String nameBadge;

	public void setBudgeValidDate(String budgeValidDate){
		this.budgeValidDate = budgeValidDate;
	}

	public String getBudgeValidDate(){
		return budgeValidDate;
	}

	public void setVisiting(String visiting){
		this.visiting = visiting;
	}

	public String getVisiting(){
		return visiting;
	}

	public void setLocation(String location){
		this.location = location;
	}

	public String getLocation(){
		return location;
	}

	public void setCText(String cText){
		this.cText = cText;
	}

	public String getCText(){
		return cText;
	}

	public void setNameBadge(String nameBadge){
		this.nameBadge = nameBadge;
	}

	public String getNameBadge(){
		return nameBadge;
	}

	@Override
 	public String toString(){
		return 
			"CheckIn{" + 
			"budgeValidDate = '" + budgeValidDate + '\'' + 
			",visiting = '" + visiting + '\'' + 
			",location = '" + location + '\'' + 
			",cText = '" + cText + '\'' + 
			",name_badge = '" + nameBadge + '\'' + 
			"}";
		}
}